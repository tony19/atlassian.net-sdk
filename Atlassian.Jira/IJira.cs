﻿using Atlassian.Jira.Linq;
using Atlassian.Jira.Remote;
using System;
using System.Collections.Generic;

namespace Atlassian.Jira
{
    /// <summary>
    /// Represents a JIRA server
    /// </summary>
    public interface IJira
    {
        /// <summary>
        /// Returns a new issue that when saved will be created on the remote JIRA server
        /// </summary>
        Issue CreateIssue(string project, string parentIssueKey = null);

        /// <summary>
        /// Whether to print the translated JQL to console
        /// </summary>
        bool Debug { get; set; }

        IFileSystem FileSystem { get; }

        /// <summary>
        /// Returns all custom fields within JIRA
        /// </summary>
        /// <returns>Collection of JIRA custom fields</returns>
        IEnumerable<JiraNamedEntity> GetCustomFields();

        IEnumerable<JiraNamedEntity> GetFieldsForEdit(string projectKey);

        /// <summary>
        /// Returns the favourite filters for the user
        /// </summary>
        IEnumerable<JiraNamedEntity> GetFilters();

        /// <summary>
        /// Gets an issue from the JIRA server
        /// </summary>
        /// <param name="key">The key of the issue</param>
        /// <returns></returns>
        Issue GetIssue(string key);

        /// <summary>
        /// Returns all the issue priorities within JIRA
        /// </summary>
        /// <returns>Collection of JIRA issue priorities</returns>
        IEnumerable<IssuePriority> GetIssuePriorities();

        /// <summary>
        /// Returns all the issue resolutions within JIRA
        /// </summary>
        /// <returns>Collection of JIRA issue resolutions</returns>
        IEnumerable<IssueResolution> GetIssueResolutions();

        /// <summary>
        /// Returns issues that match the specified filter
        /// </summary>
        /// <param name="filterName">The name of the filter used for the search</param>
        /// <param name="start">The place in the result set to use as the first issue returned</param>
        /// <param name="maxResults">The maximum number of issues to return</param>
        IEnumerable<Issue> GetIssuesFromFilter(string filterName, int start = 0, int? maxResults = null);

        /// <summary>
        /// Execute a specific JQL query and return the resulting issues
        /// </summary>
        /// <param name="jql">JQL search query</param>
        /// <returns>Collection of Issues that match the search query</returns>
        IEnumerable<Issue> GetIssuesFromJql(string jql);

        /// <summary>
        /// Execute a specific JQL query and return the resulting issues
        /// </summary>
        /// <param name="jql">JQL search query</param>
        /// <param name="maxResults">Maximum number of issues to return</param>
        /// <returns>Collection of Issues that match the search query</returns>
        IEnumerable<Issue> GetIssuesFromJql(string jql, int startAt, int maxResults);

        /// <summary>
        /// Returns all the issue statuses within JIRA
        /// </summary>
        /// <returns>Collection of JIRA issue statuses</returns>
        IEnumerable<IssueStatus> GetIssueStatuses();

        /// <summary>
        /// Returns all the issue types within JIRA
        /// </summary>
        /// <param name="projectKey">If provided, returns issue types only for given project</param>
        /// <returns>Collection of JIRA issue types</returns>
        IEnumerable<IssueType> GetIssueTypes(string projectKey = null);

        /// <summary>
        /// Returns all components defined on a JIRA project
        /// </summary>
        /// <param name="projectKey">The project to retrieve the components from</param>
        /// <returns>Collection of JIRA components</returns>
        IEnumerable<ProjectComponent> GetProjectComponents(string projectKey);

        /// <summary>
        /// Returns all projects defined in JIRA
        /// </summary>
        IEnumerable<Project> GetProjects();

        /// <summary>
        /// Returns all versions defined on a JIRA project
        /// </summary>
        /// <param name="projectKey">The project to retrieve the versions from</param>
        /// <returns>Collection of JIRA versions.</returns>
        IEnumerable<ProjectVersion> GetProjectVersions(string projectKey);

        /// <summary>
        /// Query the issues database
        /// </summary>
        /// <returns>IQueryable of Issue</returns>
        JiraQueryable<Issue> Issues { get; }

        /// <summary>
        /// Maximum number of issues per request
        /// </summary>
        int MaxIssuesPerRequest { get; set; }

        IJiraRemoteService RemoteService { get; }

        /// <summary>
        /// Url to the JIRA server
        /// </summary>
        string Url { get; }

        /// <summary>
        /// Whether to use the JIRA Rest API when querying for issues. Use only if targetting JIRA 5.0 or above.
        /// </summary>
        /// <remarks>
        /// Enables support for server side Count() and Skip() processing
        /// </remarks>
        bool UseRestApi { get; set; }

        string UserName { get; }

        string Password { get; }

        /// <summary>
        /// Executes an action using the user's authentication token and the jira soap client
        /// </summary>
        /// <remarks>
        /// If action fails with 'com.atlassian.jira.rpc.exception.RemoteAuthenticationException'
        /// a new token will be requested from server and the action called again.
        /// </remarks>
        void WithToken(Action<string, IJiraRemoteService> action);

        /// <summary>
        /// Executes an action using the user's authentication token.
        /// </summary>
        /// <remarks>
        /// If action fails with 'com.atlassian.jira.rpc.exception.RemoteAuthenticationException'
        /// a new token will be requested from server and the action called again.
        /// </remarks>
        void WithToken(Action<string> action);

        /// <summary>
        /// Executes a function using the user's authentication token.
        /// </summary>
        /// <remarks>
        /// If function fails with 'com.atlassian.jira.rpc.exception.RemoteAuthenticationException'
        /// a new token will be requested from server and the function called again.
        /// </remarks>
        TResult WithToken<TResult>(Func<string, IJiraRemoteService, TResult> function);

        /// <summary>
        /// Executes a function using the user's authentication token.
        /// </summary>
        /// <remarks>
        /// If function fails with 'com.atlassian.jira.rpc.exception.RemoteAuthenticationException'
        /// a new token will be requested from server and the function called again.
        /// </remarks>
        TResult WithToken<TResult>(Func<string, TResult> function) where TResult : class;
    }
}
