﻿using System;
namespace Atlassian.Jira
{
    public interface IIssue
    {
        void AddAttachment(params UploadAttachmentInfo[] attachments);
        void AddAttachment(params string[] filePaths);
        void AddAttachment(string name, byte[] data);
        void AddComment(string comment);
        void AddLabels(params string[] labels);
        Worklog AddWorklog(Worklog worklog, WorklogStrategy worklogStrategy = WorklogStrategy.AutoAdjustRemainingEstimate, string newEstimate = null);
        Worklog AddWorklog(string timespent, WorklogStrategy worklogStrategy = WorklogStrategy.AutoAdjustRemainingEstimate, string newEstimate = null);
        ProjectVersionCollection AffectsVersions { get; }
        string Assignee { get; set; }
        ProjectComponentCollection Components { get; }
        DateTime? Created { get; }
        CustomFieldCollection CustomFields { get; }
        void DeleteWorklog(Worklog worklog, WorklogStrategy worklogStrategy = WorklogStrategy.AutoAdjustRemainingEstimate, string newEstimate = null);
        string Description { get; set; }
        DateTime? DueDate { get; set; }
        string Environment { get; set; }
        ProjectVersionCollection FixVersions { get; }
        System.Collections.ObjectModel.ReadOnlyCollection<Attachment> GetAttachments();
        System.Collections.Generic.IEnumerable<JiraNamedEntity> GetAvailableActions();
        System.Collections.ObjectModel.ReadOnlyCollection<Comment> GetComments();
        System.Collections.ObjectModel.ReadOnlyCollection<Worklog> GetWorklogs();
        IJira Jira { get; }
        ComparableString Key { get; }
        IssuePriority Priority { get; set; }
        string Project { get; }
        void Refresh();
        string Reporter { get; set; }
        IssueResolution Resolution { get; set; }
        void SaveChanges();
        IssueStatus Status { get; set; }
        string Summary { get; set; }
        ComparableString this[string customFieldName] { get; set; }
        IssueType Type { get; set; }
        DateTime? Updated { get; }
        long? Votes { get; set; }
        void WorkflowTransition(string actionName);
    }
}
